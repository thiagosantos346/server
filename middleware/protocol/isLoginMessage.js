
const decodeLogin = require('./decodeLogin');

module.exports = function(message){
    const decodedLogin = decodeLogin(message);
    let splitedMessage = decodedLogin.split(' ');
    const command = splitedMessage[0];
    return command === 'tetrisstart';
}