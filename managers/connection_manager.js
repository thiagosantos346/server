//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');

/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */
const initConnectionManager = function(app, clientRepository){

	
	app.setOnConnectionListener((socket) => {
		if(!clientRepository.hasEmptySlot()){
			const reason = 'Server is full';
			const noConnectingMessage = `noconnecting ${reason}`;
			socket.writeTetrinetMessage(noConnectingMessage);
			return false;
		}

		return true;
	});

	app.onReceive('tetrisstart', (params, socketSender) => {
		params = String(params);
		const clientVersion = params.split(' ')[1];
		const playerName = params.split(' ')[0];
		if(clientVersion === '1.13'){

			const newClient = clientRepository.addClient(playerName, socketSender);
			let winlist = "winlist tTourTheAbyss;922 pFregge;855 pelissa;475";
			newClient.socket.writeTetrinetMessage(winlist);
	
			let loginResponse = `playernum ${newClient.id}`;
			newClient.socket.writeTetrinetMessage(loginResponse);
	
			//notify other players about connection
			const connectionMessage = `playerjoin ${newClient.id} ${newClient.name}`;
			clientRepository.broadcastAll(connectionMessage);

			//notify new client about others that are in the server
			clientRepository.connectedClients.forEach((client) => {
				const connectionMessage = `playerjoin ${client.id} ${client.name}`;
				newClient.socket.writeTetrinetMessage(connectionMessage);
			})
		}
	});

	app.onReceive('team', (params, socketSender) => {

	})

	app.setOnSocketDisconnectCallback((socket) => {
		let disconnectedClient = clientRepository.removeClientBySocket(socket);
		const message = `playerleave ${disconnectedClient.id}`;
		clientRepository.broadcastAll(message);
	});

	
}

module.exports = initConnectionManager;