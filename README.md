# Equipe de desenvolvimento do servidor

## Objetivos:
* Desenvolver um servidor TCP/tetrinet em nodejs 

## Grupo 1 - membros
* CARLOS FELIPE SPINOLA SOUZA SANTOS (líder)
* DENIS HIDEO MASUNAGA
* GABRIEL RODRIGUES DA SILVA PIRES
* JONATAS GOMES EVANGELISTA
* GUSTAVO ALVES DE SOUZA

## Tarefas da sprint 1 (25/03 a 31/03): 
* Criar protótipo inicial
* Realizar primeiros testes de um client existente (tetrinet-client ou gtetrinet) no protótipo